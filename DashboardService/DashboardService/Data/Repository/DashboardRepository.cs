﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using MongoDB.Driver;

namespace DashboardService.Data.Repository
{
    public class DashboardRepository : IRepository<Dashboard>
    {
        private readonly IMongoCollection<Dashboard> _dashboards;

        public DashboardRepository(IDbAccessor databaseAccessor)
        {
            _dashboards = databaseAccessor.Database.GetCollection<Dashboard>("dashboards");
        }

        public async Task<Dashboard> Create(Dashboard dashboard)
        {
            await _dashboards.InsertOneAsync(dashboard);
            return dashboard;
        }

        public async Task<IList<Dashboard>> Read() =>
            (await _dashboards.FindAsync(sub => true)).ToList();

        public async Task<Dashboard> Find(string id) =>
            (await _dashboards.FindAsync(aDashboard => aDashboard.Id == id)).SingleOrDefault();

        public async Task Update(Dashboard dashboard) =>
            await _dashboards.ReplaceOneAsync(aDashboard => aDashboard.Id == dashboard.Id, dashboard);

        public async Task Delete(string id) =>
            await _dashboards.DeleteOneAsync(aDashboard => aDashboard.Id == id);
    }
}
