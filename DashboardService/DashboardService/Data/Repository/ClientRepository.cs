﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using MongoDB.Driver;

namespace DashboardService.Data.Repository
{
    public class ClientRepository : IRepository<Client>
    {
        private readonly IMongoCollection<Client> _clients;

        public ClientRepository(IDbAccessor databaseAccessor)
        {
            _clients = databaseAccessor.Database.GetCollection<Client>("clients");
        }

        public async Task<Client> Create(Client client)
        {
            await _clients.InsertOneAsync(client);
            return client;
        }

        public async Task<IList<Client>> Read() =>
            (await _clients.FindAsync(sub => true)).ToList();

        public async Task<Client> Find(string id) =>
            (await _clients.FindAsync(aClient => aClient.Id == id)).SingleOrDefault();

        public async Task Update(Client client) =>
            await _clients.ReplaceOneAsync(aClient => aClient.Id == client.Id, client);

        public async Task Delete(string id) =>
            await _clients.DeleteOneAsync(aClient => aClient.Id == id);
    }
}

