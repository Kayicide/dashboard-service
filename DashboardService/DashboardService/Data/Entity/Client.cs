﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DashboardService.Data.Entity
{
    public enum ClientType
    {
        Personal = 1,
        Team = 2
    }
    public class Client
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public ClientType Type { get; set; }
        public IList<string> Admins { get; set; }
        public IList<string> Dashboards { get; set; }
        public string JoinToken { get; set; }
        public string ApiToken { get; set; }
        public bool Active { get; set; }
        public string Owner { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }
}
