﻿using System.Collections.Generic;
using DashboardService.Utility;

namespace DashboardService.Data.Entity
{
    public class User
    {
        public string Id { get; set; }
        public IList<string> ClientIds { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public SaltedHash.HashSalt HashSalt { get; set; }
        public bool Deleted { get; set; }
        public string Role { get; set; }
        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
