﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DashboardService.Data.Entity
{
    public enum XAxisTimeScale
    {
        Day = 0,
        Hour = 1,
        Minute = 2
    }
    public class Dashboard
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Name { get; set; }
        public bool ShowTotalLogs { get; set; }
        public bool ShowErrors { get; set; }
        public bool ShowWarnings { get; set; }
        public bool ShowInformation { get; set; }
        public XAxisTimeScale TimeScale { get; set; }
        public string LogIdentifier { get; set; }
        public bool LogIdentifierExact { get; set; }
        public bool Popup { get; set; }
        public int NumberToTrigger { get; set; }
    }
}
