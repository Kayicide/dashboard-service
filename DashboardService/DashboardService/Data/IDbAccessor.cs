﻿using MongoDB.Driver;

namespace DashboardService.Data
{
    public interface IDbAccessor
    {
        IMongoDatabase Database { get; }
    }
}
