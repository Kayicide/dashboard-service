﻿using MongoDB.Driver;

namespace DashboardService.Data
{
    public class DbAccessor : IDbAccessor
    {
        public IMongoDatabase Database => (new MongoClient(_settings.ConnectionString)).GetDatabase(_settings.DatabaseName);
        private readonly IDatabaseSettings _settings;
        public DbAccessor(IDatabaseSettings settings)
        {
            _settings = settings;
        }
    }
}
