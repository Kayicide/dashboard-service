﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardService.Controllers.HttpRequests
{
    public class UpdateDashboardHttpRequest
    {
        [Required]
        public string DashboardId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public bool ShowTotalLogs { get; set; }
        [Required]
        public bool ShowErrors { get; set; }
        [Required]
        public bool ShowWarnings { get; set; }
        [Required]
        public bool ShowInformation { get; set; }
        [Required]
        public int TimeScale { get; set; }

        public string LogIdentifier { get; set; } = "";
        public bool LogIdentifierExact { get; set; } = false;
        public bool Popup { get; set; } = false;
        public int NumberToTrigger { get; set; } = 10;
    }
}
