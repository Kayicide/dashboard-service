﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardService.Controllers.HttpRequests;
using DashboardService.Data.Entity;
using DashboardService.Feature;
using DashboardService.Feature.DashboardFeature;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DashboardService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class DashboardController : Controller
    {
        private readonly IRequestHandler<CreateDashboardRequest, Dashboard> _createDashboardRequestHandler;
        private readonly IRequestHandler<UpdateDashboardRequest, Dashboard> _updateDashboardRequestHandler;
        private readonly IRequestHandler<GetDashboardRequest, Dashboard> _getDashboardRequestHandler;
        private readonly IRequestHandler<GetAllDashboardsRequest, IList<Dashboard>> _getAllDashboardsRequestHandler;

        public DashboardController(IRequestHandler<CreateDashboardRequest, Dashboard> createDashboardRequestHandler, IRequestHandler<UpdateDashboardRequest, Dashboard> updateDashboardRequestHandler, IRequestHandler<GetDashboardRequest, Dashboard> getDashboardRequestHandler, IRequestHandler<GetAllDashboardsRequest, IList<Dashboard>> getAllDashboardsRequestHandler)
        {
            _createDashboardRequestHandler = createDashboardRequestHandler;
            _updateDashboardRequestHandler = updateDashboardRequestHandler;
            _getDashboardRequestHandler = getDashboardRequestHandler;
            _getAllDashboardsRequestHandler = getAllDashboardsRequestHandler;
        }

        [HttpGet]
        [Route("{dashboardId}")]
        public async Task<IActionResult> Get([FromQuery] string dashboardId)
        {
            var result = await _getDashboardRequestHandler.HandleRequest(new GetDashboardRequest{DashboardId = dashboardId});
            if (result == null)
                return BadRequest($"Dashboard with the ID: {dashboardId} cannot be found!");
            return Ok(result);
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> GetAll([FromQuery] string clientId = "")
        {
            var result = await _getAllDashboardsRequestHandler.HandleRequest(new GetAllDashboardsRequest{ClientId = clientId});
            return Ok(result);
        }

        [HttpPost]
        [Route("Create")]
        public async Task<IActionResult> Create([FromQuery] string dashboardName, [FromQuery] string clientId)
        {
            var result = await _createDashboardRequestHandler.HandleRequest(new CreateDashboardRequest{ClientId = clientId, Name = dashboardName });
            if (result == null)
                return BadRequest($"Reached the maximum amount of dashboard allowed for the client: {clientId}");
            return Ok(result);
        }

        [HttpPost]
        [Route("Update")]
        public async Task<IActionResult> Update([FromBody] UpdateDashboardHttpRequest request)
        {
            var result = await _updateDashboardRequestHandler.HandleRequest(new UpdateDashboardRequest
            {
                DashboardId = request.DashboardId,
                Name = request.Name,
                ShowWarnings = request.ShowWarnings,
                ShowTotalLogs = request.ShowTotalLogs,
                ShowErrors = request.ShowErrors,
                ShowInformation = request.ShowInformation,
                TimeScale = request.TimeScale,
                LogIdentifier = request.LogIdentifier,
                LogIdentifierExact = request.LogIdentifierExact,
                Popup = request.Popup,
                NumberToTrigger = request.NumberToTrigger
            });
            if (result == null)
                return BadRequest($"Dashboard with the ID: {request.DashboardId} cannot be found!");
            return Ok(result);
        }
    }
}
