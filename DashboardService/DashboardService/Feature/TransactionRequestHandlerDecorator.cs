﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using DashboardService.Data;

namespace DashboardService.Feature
{
    public class TransactionRequestHandlerDecorator<TRequest, TResponse> : IRequestHandler<TRequest, TResponse>
    {
        private readonly IDbAccessor _databaseAccessor;
        private readonly IRequestHandler<TRequest, TResponse> _decorated;
        public TransactionRequestHandlerDecorator(IDbAccessor databaseAccessor, IRequestHandler<TRequest, TResponse> decorated)
        {
            _databaseAccessor = databaseAccessor;
            _decorated = decorated;
        }
        public async Task<TResponse> HandleRequest(TRequest request)
        {
            using (var session = await _databaseAccessor.Database.Client.StartSessionAsync())
            {
                session.StartTransaction();
                try
                {
                    TResponse response = await _decorated.HandleRequest(request);
                    await session.CommitTransactionAsync();
                    return response;
                }
                catch (Exception ex)
                {
                    await session.AbortTransactionAsync();
                    Debug.WriteLine(ex);
                    return default;
                }
            }
        }
    }
}
