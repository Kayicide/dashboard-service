﻿using System.Threading.Tasks;
using DashboardService.Data.Entity;
using DashboardService.Data.Repository;

namespace DashboardService.Feature.DashboardFeature
{
    public class GetDashboardRequest : IRequest
    {
        public string DashboardId { get; set; }
    }
    public class GetDashboardRequestHandler : IRequestHandler<GetDashboardRequest, Dashboard>
    {
        private readonly IRepository<Dashboard> _dashboardRepository;
        public GetDashboardRequestHandler(IRepository<Dashboard> dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }
        public async Task<Dashboard> HandleRequest(GetDashboardRequest request)
        {
            return await _dashboardRepository.Find(request.DashboardId);
        }
    }
}
