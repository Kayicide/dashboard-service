﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using DashboardService.Data.Repository;

namespace DashboardService.Feature.DashboardFeature
{
    public class GetAllDashboardsRequest : IRequest
    {
        public string ClientId { get; set; } = string.Empty;
    }
    public class GetAllDashboardsRequestHandler : IRequestHandler<GetAllDashboardsRequest, IList<Dashboard>>
    {
        private readonly IRepository<Dashboard> _dashboardRepository;
        private readonly IRepository<Client> _clientRepository;
        public GetAllDashboardsRequestHandler(IRepository<Dashboard> dashboardRepository, IRepository<Client> clientRepository)
        {
            _dashboardRepository = dashboardRepository;
            _clientRepository = clientRepository;
        }
        public async Task<IList<Dashboard>> HandleRequest(GetAllDashboardsRequest request)
        {
            var dashboards = await _dashboardRepository.Read();
            if (string.IsNullOrEmpty(request.ClientId))
                return dashboards;

            var client = await _clientRepository.Find(request.ClientId);
            var dashIds = client.Dashboards;
            if (dashIds == null)
                return new List<Dashboard>();

            return dashboards.Where(x => dashIds.Contains(x.Id)).ToList();
        }
    }
}
