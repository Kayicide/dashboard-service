﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using DashboardService.Data.Repository;
using MongoDB.Bson;

namespace DashboardService.Feature.DashboardFeature
{
    public class CreateDashboardRequest : IRequest
    {
        public string Name { get; set; }
        public string ClientId { get; set; }
    }
    public class CreateDashboardRequestHandler : IRequestHandler<CreateDashboardRequest, Dashboard>
    {
        private readonly IRepository<Dashboard> _dashboardRepository;
        private readonly IRepository<Client> _clientRepository;
        public CreateDashboardRequestHandler(IRepository<Dashboard> dashboardRepository, IRepository<Client> clientRepository)
        {
            _dashboardRepository = dashboardRepository;
            _clientRepository = clientRepository;
        }

        public async Task<Dashboard> HandleRequest(CreateDashboardRequest request)
        {
            var client = await _clientRepository.Find(request.ClientId);
            if(client.Dashboards != null  && client.Dashboards.Count >= 5)
                return null;

            var newDashboard = new Dashboard
            {
                Id = ObjectId.GenerateNewId().ToString(),
                Name = request.Name,
                ShowTotalLogs = true,
                TimeScale = XAxisTimeScale.Hour,
                LogIdentifier = "",
                LogIdentifierExact = false,
                Popup = false,
                NumberToTrigger = 10
            };
            newDashboard = await _dashboardRepository.Create(newDashboard);

            client.Dashboards ??= new List<string>();
            client.Dashboards.Add(newDashboard.Id);
            await _clientRepository.Update(client);

            return newDashboard;
        }
    }
}
