﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using DashboardService.Data.Repository;

namespace DashboardService.Feature.DashboardFeature
{
    public class UpdateDashboardRequest : IRequest
    { 
        public string DashboardId { get; set; }
        public string Name { get; set; }
        public bool ShowTotalLogs { get; set; } 
        public bool ShowErrors { get; set; }
        public bool ShowWarnings { get; set; }
        public bool ShowInformation { get; set; }
        public int TimeScale { get; set; }
        public string LogIdentifier { get; set; }
        public bool LogIdentifierExact { get; set; }
        public bool Popup { get; set; }
        public int NumberToTrigger { get; set; }
    }
    public class UpdateDashboardRequestHandler : IRequestHandler<UpdateDashboardRequest, Dashboard>
    {
        private readonly IRepository<Dashboard> _dashboardRepository;
        public UpdateDashboardRequestHandler(IRepository<Dashboard> dashboardRepository)
        {
            _dashboardRepository = dashboardRepository;
        }
        public async Task<Dashboard> HandleRequest(UpdateDashboardRequest request)
        {
            var dashboard = await _dashboardRepository.Find(request.DashboardId);
            if (dashboard == null)
                return null;

            dashboard.Name = request.Name;
            dashboard.ShowTotalLogs = request.ShowTotalLogs;
            dashboard.ShowErrors = request.ShowErrors;
            dashboard.ShowWarnings = request.ShowWarnings;
            dashboard.ShowInformation = request.ShowInformation;
            dashboard.TimeScale = (XAxisTimeScale) request.TimeScale;
            dashboard.LogIdentifier = request.LogIdentifier;
            dashboard.LogIdentifierExact = request.LogIdentifierExact;
            dashboard.Popup = request.Popup;
            dashboard.NumberToTrigger = request.NumberToTrigger;

            await _dashboardRepository.Update(dashboard);

            return dashboard;
        }
    }
}
