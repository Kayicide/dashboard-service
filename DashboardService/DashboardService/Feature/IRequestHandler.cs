﻿using System.Threading.Tasks;

namespace DashboardService.Feature
{
    public interface IRequestHandler<in TRequest, TReturn>
    {
        public Task<TReturn> HandleRequest(TRequest request);
    }
}
