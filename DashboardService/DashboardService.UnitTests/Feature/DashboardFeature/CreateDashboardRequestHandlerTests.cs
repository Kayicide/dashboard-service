﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using DashboardService.Data.Repository;
using DashboardService.Feature.DashboardFeature;
using Moq;
using NUnit.Framework;

namespace DashboardService.UnitTests.Feature.DashboardFeature
{
    public class CreateDashboardRequestHandlerTests
    {
        private Mock<IRepository<Dashboard>> _dashboardRepository;
        private Mock<IRepository<Client>> _clientRepository;

        private CreateDashboardRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _dashboardRepository = new Mock<IRepository<Dashboard>>();
            _clientRepository = new Mock<IRepository<Client>>();

            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client{Dashboards = new List<string>{"", "", ""}});
            _dashboardRepository.Setup(x => x.Create(It.IsAny<Dashboard>())).ReturnsAsync(new Dashboard{Id = "Test"});

            _objectToTest = new CreateDashboardRequestHandler(_dashboardRepository.Object, _clientRepository.Object);
        }

        [Test]
        public async Task DashboardNotCreatedIfAlreadyAtFive()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client { Dashboards = new List<string> { "", "", "", "", ""} });
            var result = await _objectToTest.HandleRequest(new CreateDashboardRequest{ClientId = "Test", Name = "Test"});

            Assert.That(result, Is.Null);
        }

        [Test]
        public async Task DashboardCreatedIfAtZeroDashboards()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client());
            var result = await _objectToTest.HandleRequest(new CreateDashboardRequest { ClientId = "Test", Name = "Test" });

            Assert.That(result, !Is.Null);
        }

        [Test]
        public async Task DashboardCreatedIfAtThreeDashboards()
        {
            var result = await _objectToTest.HandleRequest(new CreateDashboardRequest { ClientId = "Test", Name = "Test" });

            Assert.That(result, !Is.Null);
        }
    }
}
