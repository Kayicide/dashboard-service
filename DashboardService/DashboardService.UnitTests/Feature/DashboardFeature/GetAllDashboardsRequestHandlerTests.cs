﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DashboardService.Data.Entity;
using DashboardService.Data.Repository;
using DashboardService.Feature.DashboardFeature;
using Moq;
using NUnit.Framework;

namespace DashboardService.UnitTests.Feature.DashboardFeature
{
    public class GetAllDashboardsRequestHandlerTests
    {
        private Mock<IRepository<Dashboard>> _dashboardRepository;
        private Mock<IRepository<Client>> _clientRepository;

        private GetAllDashboardsRequestHandler _objectToTest;

        [SetUp]
        public void Setup()
        {
            _dashboardRepository = new Mock<IRepository<Dashboard>>();
            _clientRepository = new Mock<IRepository<Client>>();

            _dashboardRepository.Setup(x => x.Read()).ReturnsAsync(new List<Dashboard>
            {
                new Dashboard {Id = "1"},
                new Dashboard {Id = "2"},
                new Dashboard {Id = "3"},
                new Dashboard {Id = "4"},
                new Dashboard {Id = "Nope"}
            });

            _objectToTest = new GetAllDashboardsRequestHandler(_dashboardRepository.Object, _clientRepository.Object);
        }

        [Test]
        public async Task AllDashboardsReturnedWhenNoClientIdSupplied()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client{Dashboards = new List<string>{"1", "2", "3", "4"}});

            var result = await _objectToTest.HandleRequest(new GetAllDashboardsRequest {ClientId = "Test"});

            Assert.That(result.Count, Is.EqualTo(4));
        }

        [Test]
        public async Task DashboardListReturnedEmptyIfClientHasNone()
        {
            _clientRepository.Setup(x => x.Find(It.IsAny<string>())).ReturnsAsync(new Client());

            var result = await _objectToTest.HandleRequest(new GetAllDashboardsRequest { ClientId = "Test" });

            Assert.That(result, !Is.Null);
            Assert.That(result.Count, Is.EqualTo(0));
        }

        [Test]
        public async Task DashboardListReturnedForClient()
        {
            var result = await _objectToTest.HandleRequest(new GetAllDashboardsRequest());

            Assert.That(result.Count, Is.EqualTo(5));
        }
    }
}
